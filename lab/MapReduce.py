# -*- coding: utf-8 -*-

from multiprocessing import Pool
from collections import defaultdict
from itertools import chain, repeat


def map_wrapper(args):
    data = args[0]
    map = args[1]
    return [x for x in map(data)]


def reduce_wrapper(args):
    data = args[0]
    reduce = args[1]
    return reduce(data[0], data[1])


def reduce_identity(key, values):
    return (key, values)


class MapReduce(object):

    def __init__(self, map_func, reduce_func=reduce_identity, num_workers=None):
        """Konstruktor MapReduce

        map_func
            Funkcja mapująca (item) -> None, wykorzystująca konstrukcję
            yield key, value
            do wygenerowania dowolnej liczby mapowań

        reduce_func=reduce_identity
            Funkcja redukująca (key, values) -> object, zwracająca
            zagregowane dane dla danego klucza. Domyślnie ustawiona jest funkcja,
            która zwraca otrzymane argumenty.

        num_workers=None
            Liczba procesów wykonujących zadanie.
            W przypadku braku parametru liczba ta jest automatycznie ustawiana
            na sumaryczną liczbę rdzeni wszystkich procesorów.
        """
        self.map_func = map_func
        self.reduce_func = reduce_func
        self.pool = Pool(num_workers)

    def __partition(self, mapped_values):
        """Organizuje zmapowane wartości po kluczu.
        Zwraca nieposortowaną sekwencję krotek z kluczem i sekwencją wartości.
        """
        partitioned_data = defaultdict(list)
        for key, value in mapped_values:
            partitioned_data[key].append(value)
        return partitioned_data.items()

    def __call__(self, inputs, chunksize=1):
        """Przetwarza dane wejściowe z użyciem funkcji map i reduce.

        inputs
            Struktura iterowalna zawierająca dane wejściowe do przetworzenia.

        chunksize=1
            Rozmiar danych wejściowych, które mają być na raz przekazane do jednego procesu.
        """
        map_responses = self.pool.map(map_wrapper, zip(inputs, repeat(self.map_func)), chunksize=chunksize)
        partitioned_data = self.__partition(chain(*map_responses))
        reduced_values = self.pool.map(reduce_wrapper, zip(partitioned_data, repeat(self.reduce_func)))
        return reduced_values
