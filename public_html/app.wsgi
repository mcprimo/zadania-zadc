# -*- coding: utf-8 -*-
import os
import requests
# This is our application object. It could have any name,
# except when using mod_wsgi where it must be "application"
import json
from multiprocessing import Pool
from collections import defaultdict
from itertools import chain, repeat
from operator import itemgetter
import sys
from wsgiref.simple_server import make_server
from cgi import parse_qs, escape

def application( # It accepts two arguments:
      # environ points to a dictionary containing CGI like environment variables
      # which is filled by the server for each received request from the client
      environ,
      # start_response is a callback function supplied by the server
      # which will be used to send the HTTP status and headers to the server
      start_response):
   d = parse_qs(environ['QUERY_STRING'])
   N = int(d.get('n', [''])[0])
   # build the response body possibly using the environ dictionary
   response_body = "<html><head><META HTTP-EQUIV='refresh' CONTENT='0.00001;URL=http://194.29.175.240/~p9/couchapp.wsgi?n="+str(N)+"'></head><body><center><h1>Please wait...</h1></center></body></html>"

   # HTTP response code and message
   status = '200 OK'

   # These are HTTP headers expected by the client.
   # They must be wrapped as a list of tupled pairs:
   # [(Header name, Header value)].
   response_headers = [('Content-Type', 'text/html'),
                       ('Content-Length', str(len(response_body)))]

   # Send them to the server using the supplied function
   start_response(status, response_headers)

   # Return the response body.
   # Notice it is wrapped in a list although it could be any iterable.
   return [response_body]